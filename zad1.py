def find_prime_sums(start_, stop_):
    plik = open("zad1.txt", "w")
    for number in range(start_, stop_ + 1):
        if jest_pierwsza(sumuj(rozdziel(str(number)))):
            plik.write(str(number)+"\n")
    plik.close()


def rozdziel(a):
    cyfry = []
    for i in a:
        cyfry.append(int(i))
    return cyfry


def sumuj(n):
    suma = 0
    for i in n:
        suma += i
    return suma


def jest_pierwsza(n):
    if n == 1:
        return False
    for i in range(2, n - 1):
        if n % i == 0:
            return False
    return True


print('podaj początek przedziału:')
start = int(input())
print('podaj koniec przedziału:')
stop = int(input())
find_prime_sums(start, stop)
