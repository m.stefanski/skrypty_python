1. [x] Napisz funkcję, która znajdzie i zapisze do pliku wszystkie liczby z zadanego przez użytkownika przedziału, których suma cyfr jest liczbą pierwszą.
2. [x] Napisz program drukujący na ekranie kalendarz na zadany miesiąc dowolnego roku (użytkownik wprowadza informację postaci: czerwiec 1997 – nazwa miesiąca w języku polskim).
3. [x] Napisz funkcję sprawdzającą, które znaki w podanym przez użytkownika zdaniu powtarzają się i uporządkuj je w kolejności od najczęściej do najrzadziej występującego.
4. Napisz funkcję, która w podanym przez użytkownika tekście znajdzie poprawny numer telefonu (cyfry mogą być oddzielone spacjami bądź znakiem -, numer może być poprzedzony znakiem +). Należy wziąć pod uwagę wszystkie „normalnie” używane formaty numerów stacjonarnych i komórkowych.
5. [x] Napisz funkcję, która przekształci wprowadzoną liczbę dziesiętną na liczbę rzymską (funkcja powinna obsługiwać liczby max pięciocyfrowe).
6. [x] Napisz funkcję, która dostaje dwie daty postaci dd.mm.rrrr i sprawdza, ile dni mija od pierwszej do drugiej z nich. Można pominąć istnienie lat przestępnych.
7. [x] Napisz funkcję, która z pliku będącego programem w języku C++ usunie wszystkie komentarze, nie pozostawiając pustych linii.
8. [x] Napisz funkcję, która dopisze 10 najczęściej występujących słów wraz z liczbą ich wystąpień na końcu każdego pliku tekstowego podanego jako parametr (dowolna liczba parametrów).
9. Napisać program, który znajdzie w podanym przez użytkownika katalogu wszystkie pliki, które zawierają w treści (nie w nazwie) zadane słowo.
10. Napisz program zwracający listę unikalnych nazw plików w danym katalogu (oczywiście z podkatalogami).
11. Napisz program, który dla pliku zawierającego listę postaci:
adres / maska / bramka
adres / maska / bramka
..... / ..... / ......
wygeneruje plik konfiguracyjny w składni dla Windows, Linuks lub Cisco (w zależności od
wyboru użytkownika) postaci np. dla Windows:
route add adres netmask maska bramka
....................................
12. [x] Napisz program, który z pliku HTML wydobędzie tekst tzn. oczyści go ze wszystkich znaczników (można zignorować elementy graficzne).
13. Napisz program realizujący prosty quiz dotyczący dowolnej dziedziny. Program powinien losować n pytań z przygotowanej puli pytań (przechowywanej w pliku) i zliczać liczbę poprawnych odpowiedzi.