#include "macierz.h"
macierz::macierz()
{
	for (int wiersz = 0; wiersz < 2; wiersz++)
	{
		for (int kolumna = 0; kolumna < 2; kolumna++)
		{
			this->tablica[wiersz][kolumna] = 0;
		}
	}
}
macierz::~macierz()
{
}
macierz::macierz(const macierz& other)
{
	for (int wiersz = 0; wiersz < 2; wiersz++)
	{
		for (int kolumna = 0; kolumna < 2; kolumna++)
		{
			tablica[wiersz][kolumna] = other.tablica[wiersz][kolumna];
		}
	}
}
void macierz::set(int wiersz, int kolumna, float wartosc)
{
	tablica[wiersz][kolumna] = wartosc;
}
macierz operator* (const macierz& macierz1, const macierz& macierz2)
{
	macierz temp;
	for (int wiersz = 0; wiersz<2; wiersz++)
	{
		for (int kolumna = 0; kolumna<2; kolumna++)
		{
			for (int pomocnicza = 0; pomocnicza < 2; pomocnicza++)
			{
				temp.tablica[wiersz][kolumna] += macierz1.tablica[wiersz][pomocnicza] * macierz2.tablica[pomocnicza][kolumna];
			}
		}
	}
	return temp;
}
bool operator== (const macierz& macierz1, const macierz& macierz2)
{
	if (macierz1.tablica[0][0] == macierz2.tablica[0][0] && macierz1.tablica[0][1] == macierz2.tablica[0][1]
		&& macierz1.tablica[1][0] == macierz2.tablica[1][0] && macierz1.tablica[1][1] == macierz2.tablica[1][1])
		return true;
	else
		return false;
}