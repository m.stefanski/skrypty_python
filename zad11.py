file = open('zad11.txt', 'r')
lines = file.readlines()

system = str(input())
dane = []
for line in lines:
    for data in line.split(" / "):
        dane.append(data.rstrip())

    if system == 'Linux':
        config = 'add '
        config += dane[0]+ '/'
        config += dane[1]
        config += ' broadcast '
        config += dane[2]
        config += ' dev eth0'
        f = open("linux.sh", "w")
        f.write(config)
        f.close()
    elif system == "Windows":
        config = 'route add '
        config += dane[0] + ' '
        config += 'netmask '
        config += dane[1] + ' ' + dane[2]
        f = open("winconfig.bat", "w")
        f.write(config)
        f.close()
    elif system == 'Cisco':
        config = 'ip address '
        config += dane[0] + ' ' + dane[1] + ' ' + dane[2]
        f = open("cisco", "w")
        f.write(config)
        f.close()


file.close()
