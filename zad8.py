import sys


# remove non alphabetic chars, to lower case
def clean_string(text):
    return ''.join(c for ch in text if ch.isalnum()).lower()


for file_name in sys.argv[1::]:
    file_input = open(file_name, 'r')
    lines = file_input.readlines()
    freq = dict()

    for line in lines:
        for c in line.split():
            if clean_string(c) in freq:
                freq[clean_string(c)] = freq[clean_string(c)] + 1
            else:
                freq[clean_string(c)] = 1

    freq_sorted = sorted(freq, key=freq.get, reverse=True)
    file_input.close()
    file_output = open(file_name, 'a')
    file_output.write('\n' + "10 most common words" + '\n')
    for c in freq_sorted[0:10]:
        file_output.write(c + ' ' + str(freq[c]) + '\n')
    file_output.close()
