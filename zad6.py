from datetime import datetime


def calculate_difference(date1, date2):
    date_format = "%d.%m.%Y"
    a = datetime.strptime(date1, date_format)
    b = datetime.strptime(date2, date_format)
    delta = b - a
    return abs(delta.days)


print('podaj pierwszą datę:')
d0 = input()
print('podaj drugą datę')
d1 = input()

print(calculate_difference(d0, d1))
