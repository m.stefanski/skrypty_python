import re


def remove_tags(text):
    return re.sub('<[^>]+>', '', text).strip()


file = open('zad12.html', 'r')
lines = file.readlines()

for line in lines:
    if len(remove_tags(line)) > 0:
        print(remove_tags(line))
