num_map = [(1000, 'M'), (900, 'CM'), (500, 'D'), (400, 'CD'), (100, 'C'), (90, 'XC'),
           (50, 'L'), (40, 'XL'), (10, 'X'), (9, 'IX'), (5, 'V'), (4, 'IV'), (1, 'I')]


def num2roman(num):
    if len(num) > 5:
        print('liczba za długa')
        exit(-1)

    roman = ''

    num_i = int(num)
    while num_i > 0:
        for i, r in num_map:
            while num_i >= i:
                roman += r
                num_i -= i

    return roman


print('podaj liczbe')
print(num2roman(input()))
