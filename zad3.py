napis = str(input())
freq = {}
for c in napis:
    freq[c] = napis.count(c)

freq_sorted = sorted(freq, key=freq.get, reverse=True)

for c in freq_sorted:
    if freq[c] > 1:
        print(c, freq[c])
