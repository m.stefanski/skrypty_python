def is_not_blank(s):
    return bool(s and s.strip())


with open("zad7.cpp", "r") as f:
    lines = f.readlines()
with open("zad7.cpp", "w") as f:
    for line in lines:
        if '//' not in line and '/*' not in line and '*/' not in line and is_not_blank(line):
            f.write(line)
