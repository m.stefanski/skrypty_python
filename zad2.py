import calendar


def daj_miesiac(mies):
    if mies == 'styczeń':
        return 1
    elif miesiac == 'luty':
        return 2
    elif miesiac == 'marzec':
        return 3
    elif miesiac == 'kwiecień':
        return 4
    elif miesiac == 'maj':
        return 5
    elif miesiac == 'czerwiec':
        return 6
    elif miesiac == 'lipiec':
        return 7
    elif miesiac == 'sierpień':
        return 8
    elif miesiac == 'wrzesień':
        return 9
    elif miesiac == 'październik':
        return 10
    elif miesiac == 'listopad':
        return 11
    elif miesiac == 'grudzień':
        return 12


print('podaj miesiąc i rok:')
data = input().split()
miesiac = data[0]
rok = int(data[1])

print(calendar.month(rok, daj_miesiac(miesiac)))
